// RGB.h: interface for the CRGB class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RGB_H__8B7497D2_83AF_4A98_9E1D_DA99CC31205B__INCLUDED_)
#define AFX_RGB_H__8B7497D2_83AF_4A98_9E1D_DA99CC31205B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRGB  
{
public:
	CRGB();
	virtual ~CRGB();
	CRGB(double,double,double);
	friend CRGB operator* (double ,const CRGB &c0);
	friend CRGB operator+ (const CRGB &c1, const CRGB &c2);
	double red,green,blue;

};

#endif // !defined(AFX_RGB_H__8B7497D2_83AF_4A98_9E1D_DA99CC31205B__INCLUDED_)
