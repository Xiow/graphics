// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__72C1FBD5_CAEB_43C3_95A0_EAD3D9AC1109__INCLUDED_)
#define AFX_STDAFX_H__72C1FBD5_CAEB_43C3_95A0_EAD3D9AC1109__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
// 版权所有(2015)
// Copyright(2015)
// 编写者: 孔令德
// Author: Kong Lingde
// 网址:www.klingde.com
// QQ:997796978
// 地址：太原工业学院 计算机工程系

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__72C1FBD5_CAEB_43C3_95A0_EAD3D9AC1109__INCLUDED_)
