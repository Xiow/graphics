// Line.cpp: implementation of the CLine class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Line.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLine::CLine()
{

}
double Round(double val)
{
    return (val> 0.0) ? floor(val+ 0.5) : ceil(val- 0.5);
}
CLine::~CLine()
{

}
void CLine::MoveTo(CDC *pDC,CP2 p0)
{
	P0 = p0;
}
void CLine::MoveTo(CDC *pDC,double x0,double y0)
{
	this->P0 = CP2(x0,y0);
}
void CLine::LineTo(CDC *pDC,CP2 p1)
{
	this->P1 = p1;
	CP2 p,t;
	COLORREF clr=RGB(0,0,0);

	if(fabs(P0.x-P1.x)==0){		//����
		if(P0.y > P1.y)
		{
			t=P0;
			P0=P1;
			P1=t;
		}
		for(p=P0;p.y < P1.y;p.y++)
		{
			pDC->SetPixelV(Round(p.x),Round(p.y),clr);
		}
	} 
	else
	{
		double k,d;
		k = (P1.y-P0.y)/(P1.x-P0.x);
		if(k>1.0)
		{
			if(P0.y>P1.y)
			{
				t=P0;
				P0=P1;
				p1=t;
			}
			d=1-0.5*k;
			for(p=P0;p.y<P1.y;p.y++)
			{
				pDC->SetPixelV(Round(p.x),Round(p.y),clr);
				if(d>=0)
				{
					p.x++;
					d+=1-k;
				}
				else
					d+=1;
			}
		}
	
		if(0.0<=k&&k<=1.0)
		{
			if(P0.x>P1.x)
			{
				t=P0;
				P0=P1;
				P1=t;
			}
			d=0.5-k;
			for(p=P0;p.x<P1.x;p.x++)
			{
				pDC->SetPixelV(Round(p.x),Round(p.y),clr);
				if(d<0)
				{
					p.y++;
					d+=1-k;
				}
				else
					d-=1-k;
			}
		}
		if(k>=-1&&k<0.0)
		{
			if(P0.x>P1.x)
			{
				t=P0;
				P0=P1;
				P1=t;
			}
			d=-0.5-k;
			for(p=P0;p.x<P1.x;p.x++)
			{
				pDC->SetPixelV(Round(p.x),Round(p.y),clr);
				if(d>0)
				{
					p.y--;
					d-=1+k;
				}
				else
					d-=k;
			}
		}
		if(k<-1.0)
		{
			if(P0.y<P1.y)
			{
				t=P0;
				P0=P1;
				P1=t;
			}
			d=-1-0.5*k;
			for(p=P0;p.y>P1.y;p.y--)
			{
				pDC->SetPixelV(Round(p.x),Round(p.y),clr);
				if(d<0)
				{
					p.x++;
					d-=1+k;
				}
				else
					d-=1;
			}
		}
	}
	P0=p1;
}
void CLine::LineTo(CDC *pDC,double x1,double y1)
{
	LineTo(pDC,CP2(x1,y1));
}