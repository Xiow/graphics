// p3_5aDoc.h : interface of the CP3_5aDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_5ADOC_H__D6EC84A1_B2EE_48C6_B3AD_FC56B989ED5F__INCLUDED_)
#define AFX_P3_5ADOC_H__D6EC84A1_B2EE_48C6_B3AD_FC56B989ED5F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_5aDoc : public CDocument
{
protected: // create from serialization only
	CP3_5aDoc();
	DECLARE_DYNCREATE(CP3_5aDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_5aDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_5aDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_5aDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_5ADOC_H__D6EC84A1_B2EE_48C6_B3AD_FC56B989ED5F__INCLUDED_)
