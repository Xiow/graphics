// p3_5aDoc.cpp : implementation of the CP3_5aDoc class
//

#include "stdafx.h"
#include "p3_5a.h"

#include "p3_5aDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP3_5aDoc

IMPLEMENT_DYNCREATE(CP3_5aDoc, CDocument)

BEGIN_MESSAGE_MAP(CP3_5aDoc, CDocument)
	//{{AFX_MSG_MAP(CP3_5aDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP3_5aDoc construction/destruction

CP3_5aDoc::CP3_5aDoc()
{
	// TODO: add one-time construction code here

}

CP3_5aDoc::~CP3_5aDoc()
{
}

BOOL CP3_5aDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CP3_5aDoc serialization

void CP3_5aDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CP3_5aDoc diagnostics

#ifdef _DEBUG
void CP3_5aDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CP3_5aDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP3_5aDoc commands
