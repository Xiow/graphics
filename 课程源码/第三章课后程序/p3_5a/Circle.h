// Circle.h: interface for the CCircle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CIRCLE_H__23D1E8D1_9818_47F1_A8EB_6F0024B8E574__INCLUDED_)
#define AFX_CIRCLE_H__23D1E8D1_9818_47F1_A8EB_6F0024B8E574__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCircle  
{
public:
	CCircle();
	void drawCircle(CDC * pDC,double,double,double);
	virtual ~CCircle();

};

#endif // !defined(AFX_CIRCLE_H__23D1E8D1_9818_47F1_A8EB_6F0024B8E574__INCLUDED_)
