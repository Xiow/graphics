// p3_9Doc.h : interface of the CP3_9Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_9DOC_H__AEB3ADC4_6B2D_4A3B_A861_7D12EB938BA6__INCLUDED_)
#define AFX_P3_9DOC_H__AEB3ADC4_6B2D_4A3B_A861_7D12EB938BA6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_9Doc : public CDocument
{
protected: // create from serialization only
	CP3_9Doc();
	DECLARE_DYNCREATE(CP3_9Doc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_9Doc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_9Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_9Doc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_9DOC_H__AEB3ADC4_6B2D_4A3B_A861_7D12EB938BA6__INCLUDED_)
