// InputDlg3.cpp : implementation file
//

#include "stdafx.h"
#include "testCallDialog.h"
#include "InputDlg3.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputDlg3 dialog


CInputDlg3::CInputDlg3(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDlg3::IDD, pParent)
{
	length = 0.0;
	 width = 0.0;
	//{{AFX_DATA_INIT(CInputDlg3)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CInputDlg3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInputDlg3)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT1, width);
	DDV_MinMaxDouble(pDX, width, 0., 1000.);
    
	DDX_Text(pDX, IDC_EDIT2, length);
	DDV_MinMaxDouble(pDX, length, 0, 1000);


}


BEGIN_MESSAGE_MAP(CInputDlg3, CDialog)
	//{{AFX_MSG_MAP(CInputDlg3)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputDlg3 message handlers
