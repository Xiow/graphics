// testCallDialogView.cpp : implementation of the CTestCallDialogView class
//

#include "stdafx.h"
#include "testCallDialog.h"
#include "InputDlg3.h"

#include "testCallDialogDoc.h"
#include "testCallDialogView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView

IMPLEMENT_DYNCREATE(CTestCallDialogView, CView)

BEGIN_MESSAGE_MAP(CTestCallDialogView, CView)
	//{{AFX_MSG_MAP(CTestCallDialogView)
	ON_COMMAND(ID_RECTANGLE, OnRectangle)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView construction/destruction

CTestCallDialogView::CTestCallDialogView()
{
	// TODO: add construction code here

}

CTestCallDialogView::~CTestCallDialogView()
{
}

BOOL CTestCallDialogView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView drawing

void CTestCallDialogView::OnDraw(CDC* pDC)
{
	CTestCallDialogDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

/*	int xa, ya, xb, yb;
	xa = 10; ya = 20;
	xb = 200; yb = 100;
	float delta_x, delta_y;
	float x,y;
	int dx,dy,steps,k;
	dx = xb - xa;
	dy = yb - ya;

	if(abs(dx) > abs(dy))
		steps = abs(dx);
	else steps = abs(dy);

	delta_x = (float)dx / (float)steps;
	delta_y = (float)dy/(float)steps;

	x = xa;
	y = ya;

	pDC->SetPixelV(x,y,RGB(255,0,0));
	for(k = 1; k <= steps; k++){
		x += delta_x;
		y += delta_y;
		pDC->SetPixelV(x,y,RGB(255,0,0));
	}
	*/

	
}

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView printing

BOOL CTestCallDialogView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestCallDialogView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestCallDialogView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView diagnostics

#ifdef _DEBUG
void CTestCallDialogView::AssertValid() const
{
	CView::AssertValid();
}

void CTestCallDialogView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestCallDialogDoc* CTestCallDialogView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestCallDialogDoc)));
	return (CTestCallDialogDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestCallDialogView message handlers

void CTestCallDialogView::OnRectangle() 
{
	// TODO: Add your command handler code here
	CInputDlg3 dlg;

	double w;
	double l;
    
	//dlg.DoModal()是显示一个对话框，并返回点击的OK,还是cancel,IDOK，IDCANCEL

 	if(IDOK==dlg.DoModal())                              //调用对话框模块，判断是否单击OK按钮
 	{
 		w=dlg.width;                                      
 		l=dlg.length;                                     
 	}
 	else
 		return;

  	RedrawWindow();                                      //重绘窗口

	CDC *pDC=GetDC(); 
	CString str = "长方形的周长为：";
	CString data;
	data.Format("%f",2*(w+l));
	str = str + data;
	pDC->TextOut(50,50,str);

	str = "长方形的面积为：";
	data;
	data.Format("%f",(w*l));
	str = str + data;
	pDC->TextOut(50,150,str);

	
	ReleaseDC(pDC);	
}
