// TaskView.h : interface of the CTaskView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TASKVIEW_H__81239641_E633_42D1_95BA_0A94AB1472EE__INCLUDED_)
#define AFX_TASKVIEW_H__81239641_E633_42D1_95BA_0A94AB1472EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CTaskView : public CView
{
protected: // create from serialization only
	CTaskView();
	DECLARE_DYNCREATE(CTaskView)

// Attributes
public:
	CTaskDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTaskView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTaskView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CPoint p0,p1;
// Generated message map functions
protected:
	//{{AFX_MSG(CTaskView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in TaskView.cpp
inline CTaskDoc* CTaskView::GetDocument()
   { return (CTaskDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TASKVIEW_H__81239641_E633_42D1_95BA_0A94AB1472EE__INCLUDED_)
