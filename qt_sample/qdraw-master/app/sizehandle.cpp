#include "sizehandle.h"
#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QPainter>
#include <qdebug.h>
#include <QtWidgets>

SizeHandleRect::SizeHandleRect(QGraphicsItem* parent , int d, bool control)
//    :QGraphicsRectItem(-SELECTION_HANDLE_SIZE/2,
//                       -SELECTION_HANDLE_SIZE/2,
//                       SELECTION_HANDLE_SIZE,
//                       SELECTION_HANDLE_SIZE,parent)
    :QGraphicsItem(parent)
    ,m_dir(d)
    ,m_controlPoint(control)
    ,m_state(SelectionHandleOff)
    ,borderColor("white")
{
    //setFlags(QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    //setAcceptHoverEvents(true);
    //这个很重要，不跟其他对象一超坐标变换！！
    setFlag(QGraphicsItem::ItemIgnoresTransformations,true);
    hide();
}

QRectF SizeHandleRect::boundingRect() const
{
    return QRect(-SELECTION_HANDLE_SIZE/2,
                       -SELECTION_HANDLE_SIZE/2,
                       SELECTION_HANDLE_SIZE,
                       SELECTION_HANDLE_SIZE);

}
void SizeHandleRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->save();
    painter->setPen(Qt::SolidLine);
    painter->setBrush(QBrush(borderColor));
    //qDebug()<<"SizeHandleRect="<<rect();
    painter->setRenderHint(QPainter::Antialiasing,false);

    if ( m_controlPoint )
    {
        painter->setPen(QPen(Qt::red,Qt::SolidLine));
        painter->setBrush(Qt::green);
        painter->drawEllipse(boundingRect().center(),boundingRect().width()/2,boundingRect().height()/2);
    }else
        painter->drawRect(boundingRect());
    painter->restore();
}


void SizeHandleRect::setState(SelectionHandleState st)
{
    if (st == m_state)
        return;
    switch (st) {
    case SelectionHandleOff:
        hide();
        break;
    case SelectionHandleInactive:
    case SelectionHandleActive:
        show();
        break;
    }
    borderColor = Qt::white;
    m_state = st;
}

void SizeHandleRect::move(qreal x, qreal y)
{   
    setPos(x,y);
}

void SizeHandleRect::hoverEnterEvent(QGraphicsSceneHoverEvent *e)
{
    //setCursor(Qt::OpenHandCursor); //设置光标为手张开的形状
    //setToolTip("I am item");
    //return;

    //qDebug()<<"e->pos()="<<e->pos()<<",rect="<<boundingRect();
    borderColor = Qt::blue;
    update();

    QGraphicsItem::hoverEnterEvent(e);
}

void SizeHandleRect::hoverLeaveEvent(QGraphicsSceneHoverEvent *e)
{
    borderColor = Qt::white;
    update();
    QGraphicsItem::hoverLeaveEvent(e);
}







