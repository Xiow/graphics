
// TestView.cpp : CTestView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Test.h"
#endif

#include "TestDoc.h"
#include "TestView.h"
#include "math.h"//包含数学头文件
#define  PI 3.1415926//PI的宏定义
#define Round(d) int(floor(d+0.5))//四舍五入宏定义
#define LEFT   1   //代表:0001
#define RIGHT  2   //代表:0010
#define BOTTOM 4   //代表:0100
#define TOP    8   //代表:1000
#define  InMax   7  //最大输入顶点数
#define  OutMax  12 //最大输出顶点数
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
//	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_LBUTTONDOWN()
//	ON_WM_LBUTTONUP()
//	ON_WM_MOUSEMOVE()
//	ON_COMMAND(IDM_CLIP, &CTestView::OnClip)
//	ON_UPDATE_COMMAND_UI(IDM_CLIP, &CTestView::OnUpdateClip)
	ON_WM_ERASEBKGND()
//	ON_WM_RBUTTONDOWN()
//	ON_COMMAND(IDM_ZOOMIN, &CTestView::OnZoomin)
//	ON_COMMAND(IDM_ZOOMOUT, &CTestView::OnZoomout)
	ON_COMMAND(IDM_CLIP, &CTestView::OnClip)
	ON_UPDATE_COMMAND_UI(IDM_CLIP, &CTestView::OnUpdateClip)
	ON_COMMAND(IDM_DRAWPIC, &CTestView::OnDrawpic)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

// CTestView 构造/析构

CTestView::CTestView()
{
	// TODO: 在此处添加构造代码
	bDrawRect=FALSE;
	bClip=FALSE;
	OutCount=0;
	RtCount=0;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CTestView 绘制

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
	DoubleBuffer(pDC);
}


// CTestView 打印

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}


// CTestView 诊断

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG


// CTestView 消息处理程序
void CTestView::ReadPoint()//点表
{
	In=new CP2[InMax];//输入顶点表
	In[0].x=50;  In[0].y=100;
	In[1].x=-150;In[1].y=300;
	In[2].x=-250;In[2].y=50;
	In[3].x=-150;In[3].y=-250;
	In[4].x=0;   In[4].y=-50;
	In[5].x=100; In[5].y=-250;
	In[6].x=300; In[6].y=150;
	Out=new CP2[OutMax];//输出顶点表
	for(int i=0;i<OutMax;i++)
		Out[i]=CP2(0,0);
}
void CTestView::DoubleBuffer(CDC *pDC)//双缓冲
{
	CRect rect;//定义客户区
	GetClientRect(&rect);//获得客户区的大小
	pDC->SetMapMode(MM_ANISOTROPIC);//pDC自定义坐标系
	pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口范围
	pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区范围,x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);//客户区中心为原点
	CDC memDC;//内存DC
	CBitmap NewBitmap,*pOldBitmap;//内存中承载的临时位图
	memDC.CreateCompatibleDC(pDC);//创建一个与显示pDC兼容的内存memDC 
	NewBitmap.CreateCompatibleBitmap(pDC,rect.Width(),rect.Height());//创建兼容位图 
	pOldBitmap=memDC.SelectObject(&NewBitmap);//将兼容位图选入memDC 
	memDC.FillSolidRect(rect,pDC->GetBkColor());//按原来背景填充客户区，否则是黑色
	memDC.SetMapMode(MM_ANISOTROPIC);//memDC自定义坐标系
	memDC.SetWindowExt(rect.Width(),rect.Height());
	memDC.SetViewportExt(rect.Width(),-rect.Height());
	memDC.SetViewportOrg(rect.Width()/2,rect.Height()/2);
	rect.OffsetRect(-rect.Width()/2,-rect.Height()/2);
	if(RtCount && !bClip)
		DrawWindowRect(&memDC);//绘制窗口
	DrawObject(&memDC,bClip);//绘制多边形
	pDC->BitBlt(rect.left,rect.top,rect.Width(),rect.Height(),&memDC,-rect.Width()/2,-rect.Height()/2,SRCCOPY);//将内存memDC中的位图拷贝到显示pDC中
	memDC.SelectObject(pOldBitmap);//恢复位图
	NewBitmap.DeleteObject();//删除位图
}

CP2 CTestView::Convert(CPoint point)//坐标系变换 
{
	CRect rect;
	GetClientRect(&rect);
	CP2 ptemp;
	ptemp.x=point.x-rect.Width()/2;
	ptemp.y=rect.Height()/2-point.y;
	return ptemp;
}
void CTestView::DrawWindowRect(CDC* pDC)//绘制裁剪窗口
{
	// TODO: Add your message handler code here and/or call default
	CPen NewPen3,*pOldPen3;//定义3个像素宽度的画笔
	NewPen3.CreatePen(PS_SOLID,3,RGB(0,128,0));
	pOldPen3=pDC->SelectObject(&NewPen3);
	pDC->MoveTo(Round(Rect[0].x),Round(Rect[0].y));
	pDC->LineTo(Round(Rect[1].x),Round(Rect[0].y));
	pDC->LineTo(Round(Rect[1].x),Round(Rect[1].y));
	pDC->LineTo(Round(Rect[0].x),Round(Rect[1].y));
	pDC->LineTo(Round(Rect[0].x),Round(Rect[0].y));
	pDC->SelectObject(pOldPen3);
	NewPen3.DeleteObject();
}

BOOL CTestView::Inside(CP2 p,UINT Boundary)//判断点在窗口的内外
{
	switch(Boundary)
	{
	case LEFT:
		if(p.x>=Wxl)
			return TRUE;
		break;
	case RIGHT:
		if(p.x<=Wxr)
			return TRUE;
		break;
	case TOP:
		if(p.y<=Wyt)
			return TRUE;
		break;
	case BOTTOM:
		if(p.y>=Wyb)
			return TRUE;
		break;
	}
	return FALSE;
}
void CTestView::ClipPolygon(CP2 *out,int Length,UINT Boundary)//裁剪多边形
{
	if(0==Length)
		return;
	CP2 *pTemp=new CP2[Length];
	for(int i=0;i<Length;i++)
		pTemp[i]=out[i];
	CP2 p0,p1,p;//p0-起点，p1-终点，p-交点
	OutCount=0;
	p0=pTemp[Length-1];
	for(int i=0;i<Length;i++)
	{
		p1=pTemp[i];
		if(Inside(p0,Boundary))//起点在窗口内
		{
			if(Inside(p1,Boundary))//终点在窗口内,属于内→内
			{
				Out[OutCount]=p1;//终点在窗口内
				OutCount++;
			}
			else//属于内→外
			{
				p=Intersect(p0,p1,Boundary);//求交点
				Out[OutCount]=p;
				OutCount++;
			}
		}
		else if(Inside(p1,Boundary))//终点在窗口内,属于外→内
		{
			p=Intersect(p0,p1,Boundary);//求交点
			Out[OutCount]=p;
			OutCount++;
			Out[OutCount]=p1;
			OutCount++;
		}
		p0=p1;
	}
	delete[] pTemp;
}
CP2 CTestView::Intersect(CP2 p0,CP2 p1,UINT Boundary)//求交点
{
	CP2 pTemp;
	double k=(p1.y-p0.y)/(p1.x-p0.x);//直线段的斜率
	switch(Boundary)
	{
	case LEFT:
		pTemp.x=Wxl;
		pTemp.y=k*(pTemp.x-p0.x)+p0.y;
		break;
	case RIGHT:		
		pTemp.x=Wxr;
		pTemp.y=k*(pTemp.x-p0.x)+p0.y;
		break;
	case TOP:
		pTemp.y=Wyt;
		pTemp.x=(pTemp.y-p0.y)/k+p0.x;
		break;
	case BOTTOM:
		pTemp.y=Wyb;
		pTemp.x=(pTemp.y-p0.y)/k+p0.x;
		break;
	}
	return pTemp;
}

void CTestView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	if(bDrawRect)
	{	
		if(RtCount<2)
		{
			Rect[RtCount]=Convert(point);
    		RtCount++;
		}	
	}
	CView::OnLButtonDown(nFlags, point);
}

BOOL CTestView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	return TRUE;
	//return CView::OnEraseBkgnd(pDC);
}


void CTestView::OnClip()
{
	// TODO: 在此添加命令处理程序代码
	RtCount=0;
	bDrawRect=FALSE;
	bClip=TRUE;
	ClipBoundary(Rect[0],Rect[1]);
	Invalidate(FALSE);
}


void CTestView::OnUpdateClip(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
	pCmdUI->Enable((RtCount>=2)?TRUE:FALSE);
}


void CTestView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	// TODO: 在此添加专用代码和/或调用基类
	ReadPoint();
}
void CTestView::DrawObject(CDC *pDC,BOOL bclip)//绘制多边形
{
	if(!bclip)
	{
		for(int i=0;i<InMax;i++)//绘制裁剪前的多边形
		{
			if(0==i)
				pDC->MoveTo(Round(In[i].x),Round(In[i].y));
		    else
		    	pDC->LineTo(Round(In[i].x),Round(In[i].y));
		}
    	pDC->LineTo(Round(In[0].x),Round(In[0].y));
	}
	else
	{
		ClipPolygon(In,InMax,LEFT);
		ClipPolygon(Out,OutCount,RIGHT);
		ClipPolygon(Out,OutCount,BOTTOM);
		ClipPolygon(Out,OutCount,TOP);
		for(int j=0;j<OutCount;j++)//绘制裁剪后的多边形
		{
			if(0==j)
				pDC->MoveTo(Round(Out[j].x),Round(Out[j].y));
		    else
		    	pDC->LineTo(Round(Out[j].x),Round(Out[j].y));
		}
		if(0!=OutCount)
			pDC->LineTo(Round(Out[0].x),Round(Out[0].y));
	}
}
void CTestView::ClipBoundary(CP2 rect0,CP2 rect1)//窗口边界赋值函数
{
	if(rect0.x>rect1.x)
	{
		Wxl=rect1.x;
		Wxr=rect0.x;
	}
	else
	{
		Wxl=rect0.x;
		Wxr=rect1.x;
	}
	if(rect0.y>rect1.y)
	{
		Wyt=rect0.y;
		Wyb=rect1.y;
	}
	else
	{
		Wyt=rect1.y;
		Wyb=rect0.y;
	}
}
void CTestView::OnDrawpic()
{
	// TODO: 在此添加命令处理程序代码
		RtCount=0;
	bDrawRect=TRUE;
	bClip=FALSE;
	Rect[0]=Rect[1]=CP2(0,0);
	Invalidate(FALSE);
	MessageBox(CString("鼠标绘制窗口，剪刀进行裁剪"),CString("提示"),MB_OKCANCEL);
}


void CTestView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(!bClip)
	{
		if(RtCount<2)
		{   
			Rect[RtCount]=Convert(point);
			Invalidate(FALSE);
		}
	}

	CView::OnMouseMove(nFlags, point);
}
