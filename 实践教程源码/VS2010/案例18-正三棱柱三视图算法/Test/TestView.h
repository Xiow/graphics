
// TestView.h : CTestView 类的接口
//

#pragma once

#include "Face.h"//包含面头文件
#include "Line.h"//包含直线头文件
#include "P3.h"//包含直线头文件
class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void ReadPoint();//读入顶点函数
	void ReadFace();//读入面函数
	void DoubleBuffer(CDC *pDC);//双缓冲绘图
	void DrawObject(CDC *,CP3 []);//绘制斜等测图线框模型
	void DrawTriView(CDC *,CP3 []);//绘制三视图线框模型
	void Rotate();//旋转变换
	void TVMatrix();//读入主视图变换矩阵
	void THMatrix();//读入俯视图变换矩阵
	void TWMatrix();//读入侧视图变换矩阵
	void TOMatrix();//读入斜等测变换矩阵
	void MultiMatrix(double T[][4]);//矩阵相乘 
	void DrawOblique(CDC *pDC);//绘制斜等侧图
	void DrawVView(CDC *pDC );//绘制主视图
	void DrawHView(CDC *pDC);//绘制俯视图
	void DrawWView(CDC *pDC);//绘制侧视图
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CP3 P[6];//三维顶点
	CP3 PNew[6];//三视图顶点
	CFace F[5];//面表
	double TV[4][4];//主视图变换矩阵
	double TH[4][4];//俯视图变换矩阵
	double TW[4][4];//侧视图变换矩阵
	double TO[4][4];//斜等测变换矩阵	
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void OnInitialUpdate();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

