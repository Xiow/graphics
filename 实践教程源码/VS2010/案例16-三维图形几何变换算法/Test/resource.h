//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Test.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_TestTYPE                    130
#define IDD_DIALOG1                     310
#define IDB_BITMAP1                     311
#define IDI_ICON1                       312
#define IDI_ICON2                       313
#define IDI_ICON3                       314
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define ID_BUTTON32771                  32771
#define ID_BUTTON32772                  32772
#define ID_BUTTON32773                  32773
#define ID_DRAWPIC                      32774
#define IDM_DRAWPIC                     32775
#define ID_TLEFT                        32776
#define ID_TRIGHT                       32777
#define ID_TUP                          32778
#define ID_TDOWN                        32779
#define ID_TFRONT                       32780
#define ID_TBACK                        32781
#define ID_SINCREASE                    32782
#define ID_SDECREASE                    32783
#define ID_RXAXIS                       32784
#define ID_RYAXIS                       32785
#define ID_RZAXIS                       32786
#define ID_RXOY                         32787
#define ID_RYOZ                         32788
#define ID_RXOZ                         32789
#define ID_SXDIRECTIONPLUS              32790
#define ID_SYDIRECTIONPLUS              32791
#define ID_SZDIRECTIONNEG               32792
#define ID_BUTTON32793                  32793
#define ID_RESET                        32793

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        315
#define _APS_NEXT_COMMAND_VALUE         32795
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
