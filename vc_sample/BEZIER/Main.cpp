#include <afxwin.h>
#include "Bezier.h"	// Added by ClassView
class CMyWindow:public CFrameWnd
{
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
private:
	BOOL rotating;
	int nMove;
	BOOL bBezierMove;
	CBezier m_Bezier;
	CPoint OldPos;
	CPoint startPoint;
public:
	CMyWindow();
};
CMyWindow::CMyWindow()
{
	nMove=-1;
	bBezierMove=FALSE;
}
BEGIN_MESSAGE_MAP(CMyWindow,CFrameWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()
void CMyWindow::OnMouseMove(UINT nFlags, CPoint point)
{
	rotating=false;
	if(nMove!=-1)
	{
		m_Bezier.setPoint(nMove,point);
		Invalidate();
	}
	else if(bBezierMove)
	{		
		m_Bezier.Move(point.x-OldPos.x,point.y-OldPos.y);
		OldPos=point;
		Invalidate();
	}
	else if((nFlags & MK_LBUTTON)==MK_LBUTTON)//如果是在旋转
	{
		rotating=true;
		m_Bezier.Rotate(startPoint,point);
		startPoint=point;
		Invalidate();
	} 
}
void CMyWindow::OnLButtonDown(UINT nFlags, CPoint point)
{
	UINT i;
	CRect rec;
	int RecWidth=5;
	//计算中心点坐标
	m_Bezier.CalcCenter();
	CClientDC dc(this);

	//dc.SetMapMode(MM_ISOTROPIC);
	//dc.SetWindowExt(1024,768);
	//dc.SetViewportExt(1024*2,768*2);
	//dc.DPtoLP(&point);
	startPoint=point;
	for(i=0;i<m_Bezier.m_nPoint;i++)
	{
		rec.left=m_Bezier.m_Point[i].x-RecWidth;
		rec.top=m_Bezier.m_Point[i].y-RecWidth;
		rec.right=m_Bezier.m_Point[i].x+RecWidth;
		rec.bottom=m_Bezier.m_Point[i].y+RecWidth;
		if(PtInRect(&rec,point)	)
		{
			nMove=i;
			break;
		}
		else
		{
			nMove=-1;
			
			if(m_Bezier.PtInBezier(point))
			{
				bBezierMove=TRUE;
				OldPos=point;
			}
			else bBezierMove=FALSE;
		}
	}

}
void CMyWindow::OnLButtonUp(UINT nFlags, CPoint point)
{
	nMove=-1;
	bBezierMove=FALSE;
	rotating=FALSE;
	m_Bezier.CalcCenter();
	Invalidate();
}
void CMyWindow::OnPaint()
{
	CClientDC dc(this);
	
	//dc.SetMapMode(MM_ISOTROPIC);
	//dc.SetWindowExt(1024,768);
	//dc.SetViewportExt(1024*2,768*2);
	m_Bezier.Draw(&dc);
	if(rotating)
	{
		dc.MoveTo(m_Bezier.m_Center);
		dc.LineTo(startPoint);
	}
}

class CMyApp:public CWinApp
{
	BOOL InitInstance();
};

BOOL CMyApp::InitInstance()
{
	CMyWindow *wnd=new CMyWindow();
	wnd->Create(0,"Draw Bezier Line,点击控制点或曲线可以拖动,点击其他地方可以旋转",WS_OVERLAPPEDWINDOW);
	//wnd->ShowWindow(m_nCmdShow);
	wnd->ShowWindow(SW_SHOWMAXIMIZED);
	m_pMainWnd=wnd;
	return TRUE;
}

CMyApp theApp;